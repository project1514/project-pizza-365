
$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    const gCOL_ODERID = 0;
    const gCOL_SIZE = 1;
    const gCOL_TYPE = 2;
    const gCOL_DRINK = 3;
    const gCOL_PRICE = 4;
    const gCOL_NAME = 5;
    const gCOL_PHONE = 6;
    const gCOL_STATUS = 7;
    const gCOL_ACTION = 8;

    var gCol_Data = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"]

    var gTable = $("#oder-pizza-table").DataTable({
        columns: [
            { data: gCol_Data[gCOL_ODERID] },
            { data: gCol_Data[gCOL_SIZE] },
            { data: gCol_Data[gCOL_TYPE] },
            { data: gCol_Data[gCOL_DRINK] },
            { data: gCol_Data[gCOL_PRICE] },
            { data: gCol_Data[gCOL_NAME] },
            { data: gCol_Data[gCOL_PHONE] },
            { data: gCol_Data[gCOL_STATUS] },
            { data: gCol_Data[gCOL_ACTION] }
        ],
        columnDefs: [
            {
                targets: gCOL_ACTION,
                defaultContent: `
    <button class="btn btn-link btn-detail" data-toggle="tooltip" data-placement="bottom" title="Edit Course"><i class="fa-solid fa-pen-to-square text-primary pointer"></i></button>
    `
            }
        ]
    });
    var gOdersPizzaDB;
    var gId;
    var gDiscount;
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading()
    // gán sự kiện chọn size load dữ liệu size vào form create
    getDataSize();
    getVoucherDiscount();
    // gán sự kiện nút chi tiết
    $("#oder-pizza-table").on("click", ".btn-detail", function () {
        onBtnDetailClick(this);
    });
    // gán sự kiện nút lọc 
    $("#btn-filter").on("click", function () {
        onBtnFilterClick();
    });
    // gán sự kiện nút xác nhận confirm
    $("#btn-confimr").on("click", function () {
        onBtnConfirmClick();
    });
    // gán sự kiện nút hủy bỏ cancel
    $("#btn-cancel").on("click", function () {
        $("#detail-modal").modal("hide");
    });
    // gán sự kiện nút Thêm được ấn
    $("#btn-create").on("click", function () {
        onBtnCreateClick();
    });
    // gán sự kiện nút xác nhận tạo đơn 
    $("#btn-confimr-create").on("click", function () {
        onBtnAcceptCreateClick();
    });
    $("#btn-cancel-create").on("click", function () {
        $("#create-modal").modal("hide")
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm tải lại trang
    function onPageLoading() {
        "use strict";
        // lấy dữ liệu từ server
        getDataApi()
        getDrinkList()
    }
    // hàm xử lý sự kiện chọn size load dữ liệu size 
    function getDataSize() {
        "use strict";
        var vSizeS = [20, 2, 200, 2, 150000];
        var vSizeM = [25, 4, 300, 3, 200000];
        var vSizeL = [30, 8, 500, 4, 250000];
        $("#select-size-create").on("change", function () {
            onLoadDataSizeForm(vSizeS, vSizeM, vSizeL); // gán sự kiện load dữ liệu vào vào form create
        })
    }
    function getVoucherDiscount() {
        "use strict";
        $("#inp-voucher-id-create").on("change", function () {
            onLoadDataForm(); // gán sự kiện load dữ liệu vào vào form create
        })
    }
    function onLoadDataForm() {
        "use strict";
        var vInputVoucher = $("#inp-voucher-id-create").val();
        if (vInputVoucher) {
            checkVoucherDiscount(vInputVoucher);
        }
    }
    // hàm xử lý sự kiện load dữ liệu vào vào form create
    function onLoadDataSizeForm(paramS, paramM, paramL) {
        "use strict";
        if ($("#select-size-create").val() == "S") {
            $("#inp-duong-kinh-create").val(paramS[0]);
            $("#inp-suonnuong-create").val(paramS[1])
            $("#inp-salad-create").val(paramS[2])
            $("#inp-soluongnuocuong-create").val(paramS[3])
            $("#inp-price-create").val(paramS[4])
        }
        if ($("#select-size-create").val() == "M") {
            $("#inp-duong-kinh-create").val(paramM[0]);
            $("#inp-suonnuong-create").val(paramM[1])
            $("#inp-salad-create").val(paramM[2])
            $("#inp-soluongnuocuong-create").val(paramM[3])
            $("#inp-price-create").val(paramM[4])
        }
        if ($("#select-size-create").val() == "L") {
            $("#inp-duong-kinh-create").val(paramL[0]);
            $("#inp-suonnuong-create").val(paramL[1])
            $("#inp-salad-create").val(paramL[2])
            $("#inp-soluongnuocuong-create").val(paramL[3])
            $("#inp-price-create").val(paramL[4])
        }
    }
    // hàm xử lý sự kiện lấy dữ liệu từ server về
    function getDataApi() {
        "use strict";
        $.ajax({
            url: gBASE_URL,
            type: "GET",
            async: false,
            success: function (Response) {
                gOdersPizzaDB = Response;
                console.log(gOdersPizzaDB);
                onLoadDataToTable(gOdersPizzaDB) // sự kiện load dữ liệu vào bảng
            }
        })
    }
    // hàm xử lý sự kiện nút chi tiết được ấn
    function onBtnDetailClick(paramButton) {
        "use strict";
        var vTable = $("#oder-pizza-table").DataTable();
        var vRowClick = $(paramButton).closest("tr");
        var vDataRow = vTable.row(vRowClick).data();
        console.log(vDataRow);
        gId = vDataRow.id;
        $("#detail-modal").modal();
        loadDataFormDetail(vDataRow); // gán sự kiện load dữ liệu vào form modal detail
    }
    // hàm xử lý sự kiện khi nút lọc được ấn
    function onBtnFilterClick() {
        "use strict";
        // thu thập dữ liệu
        var vFilter = {
            status: "",
            typePizza: ""
        };
        getData(vFilter);
        // kiểm tra dữ liệu 
        var vIsCheck = isCheckData(vFilter)
        if (vIsCheck) {
            showDataResult(vFilter) // Hiển thị kết qua Filter
        }
    }
    // hàm xử lý sự kiện thu thập dữ liệu
    function getData(paramFilter) {
        "use strict";
        paramFilter.status = $("#select-status").val();
        paramFilter.typePizza = $("#select-type-pizza").val();
    }
    // hàm xử lý sự kiện kiểm tra dữ liệu
    function isCheckData(paramFilter) {
        "use strict";
        if (paramFilter.status == "" & paramFilter.typePizza == "") {
            onLoadDataToTable(gOdersPizzaDB);
            return false;
        }
        return true;
    }
    // hàm xử lý sự kiện hiển thị kết quả Filter
    function showDataResult(paramFilter) {
        "use strict";
        var gOdersPizzaDBF = gOdersPizzaDB.filter(function (paramOderPizza) {
            if (paramOderPizza.loaiPizza != null) {
                return ((paramFilter.status === "" || paramOderPizza.trangThai.toLowerCase().includes(paramFilter.status.toLowerCase()))
                    && (paramFilter.typePizza === "" || paramOderPizza.loaiPizza.toLowerCase().includes(paramFilter.typePizza.toLowerCase())))
            }
        });
        onLoadDataToTable(gOdersPizzaDBF);
    }
    // hàm xử lý sự kiện load dữ liệu vào form modal detail
    function loadDataFormDetail(paramData) {
        "use strict";
        $("#inp-oder-id").val(paramData.orderId);
        $("#select-size").val(paramData.kichCo);
        $("#inp-duong-kinh").val(paramData.duongKinh);
        $("#inp-suonnuong").val(paramData.suon);
        $("#select-drink").val(paramData.idLoaiNuocUong);
        $("#inp-soluongnuocuong").val(paramData.soLuongNuoc);
        $("#inp-voucher-id").val(paramData.idVourcher);
        $("#inp-pizza-type").val(paramData.loaiPizza);
        $("#inp-salad").val(paramData.salad);
        $("#inp-price").val(paramData.thanhTien);
        $("#inp-discount").val(paramData.giamGia);
        $("#inp-name").val(paramData.hoTen);
        $("#inp-email").val(paramData.email);
        $("#inp-phone").val(paramData.soDienThoai);
        $("#inp-address").val(paramData.diaChi);
        $("#inp-message").val(paramData.loiNhan);
        $("#select-status-detail").val(paramData.trangThai);
        $("#inp-date-order").val(paramData.ngayTao);
        $("#inp-update-date").val(paramData.ngayCapNhat);
    }
    // hàm xử lý sự kiện nút confirm được ấn
    function onBtnConfirmClick() {
        "use strict";
        console.log(gId);
        var vStatus = {
            trangThai: ""
        }
        readDataConfirm(vStatus) // sự kiện đọc dữ liệu Xác nhận hoặc hủy bỏ
        callApiConfirmOrder(vStatus, gId) // sự kiện gọi server cập nhật đơn hàng
    }
    // hàm xử lý sự kiện đọc dữ liệu Xác nhận hoặc hủy bỏ
    function readDataConfirm(paramStatus) {
        "use strict";
        paramStatus.trangThai = $("#select-status-detail").val();
    }
    // hàm xử lý sự kiện gọi server cập nhật đơn hàng
    function callApiConfirmOrder(paramStatus, paramId) {
        "use strict";
        $.ajax({
            url: gBASE_URL + "/" + paramId,
            type: "PUT",
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramStatus),
            success: function (Response) {
                alert("Xác nhận đơn hàng thành công")
                console.log(Response);
                resetDetailForm(); // sự kiện reset lại detail form và load lại bảng
            },
            error: function () {
                alert("Lỗi");
            }
        })
    }
    // hàm xử lý sự kiện reset lại detail form và load lại bảng
    function resetDetailForm() {
        "use strict";
        $("#detail-modal").modal("hide");
        $("#inp-oder-id").val("");
        $("#select-size").val("");
        $("#inp-duong-kinh").val("");
        $("#inp-suonnuong").val("");
        $("#select-drink").val("");
        $("#inp-soluongnuocuong").val("");
        $("#inp-voucher-id").val("");
        $("#inp-pizza-type").val("");
        $("#inp-salad").val("");
        $("#inp-price").val("");
        $("#inp-discount").val("");
        $("#inp-name").val("");
        $("#inp-email").val("");
        $("#inp-phone").val("");
        $("#inp-address").val("");
        $("#inp-message").val("");
        $("#inp-status").val("");
        $("#inp-date-order").val("");
        $("#inp-update-date").val("");
        getDataApi();
        onLoadDataToTable(gOdersPizzaDB);
    }
    // hàm xử lý sự kiện nút thêm được ấn
    function onBtnCreateClick() {
        "use strict";
        $("#create-modal").modal();
    }
    // hàm xử lý sự kiện nút xác nhận tạo đơn được ấn
    function onBtnAcceptCreateClick() {
        "use strict";
        var vDataOder = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        };
        // thu thập dữ liệu Accept
        readDataAccept(vDataOder);
        // kiểm tra dữ liệu nhập
        var vIsCheckDataCreate = checkDataCreateForm(vDataOder);
        if (vIsCheckDataCreate) {
            // Gọi server tạo đơn hàng
            callApiPostOrder(vDataOder);
        }
    }
    // hàm xử lý sự kiện thu thập dữ liệu Accept
    function readDataAccept(paramDataOrder) {
        "use strict";
        paramDataOrder.kichCo = $("#select-size-create").val();
        paramDataOrder.duongKinh = $("#inp-duong-kinh-create").val();
        paramDataOrder.suon = $("#inp-suonnuong-create").val();
        paramDataOrder.salad = $("#inp-salad-create").val();
        paramDataOrder.loaiPizza = $("#select-pizza-create").val();
        paramDataOrder.idVourcher = $("#inp-voucher-id-create").val();
        paramDataOrder.idLoaiNuocUong = $("#select-drink-create").val();
        paramDataOrder.soLuongNuoc = $("#inp-soluongnuocuong-create").val();
        paramDataOrder.hoTen = $("#inp-name-create").val();
        paramDataOrder.thanhTien = $("#inp-price-create").val();
        paramDataOrder.email = $("#inp-discount-create").val();
        paramDataOrder.soDienThoai = $("#inp-phone-create").val();
        paramDataOrder.diaChi = $("#inp-address-create").val();
        paramDataOrder.loiNhan = $("#inp-message-create").val();
        // console.log(paramDataOrder);
    }
    // hàm xử lý sự kiện kiểm tra dữ liệu nhập
    function checkDataCreateForm(paramDataOrder) {
        "use strict";
        if (paramDataOrder.kichCo == "") {
            alert("Bạn cần chọn size Pizza");
            return false;
        }
        if (paramDataOrder.loaiPizza == "") {
            alert("Bạn cần chọn loại Pizza");
            return false;
        }
        if (paramDataOrder.idLoaiNuocUong == "") {
            alert("Bạn cần chọn loại nước uống");
            return false;
        }
        if (paramDataOrder.hoTen == "") {
            alert("Bạn cần điền họ tên");
            return false;
        }
        if (paramDataOrder.soDienThoai == "") {
            alert("Bạn cần điền số điện thoại");
            return false;
        }
        if (paramDataOrder.diaChi == "") {
            alert("Bạn cần điền địa chỉ");
            return false;
        }
        if (isNaN(paramDataOrder.soDienThoai)) {
            alert("Số điền thoại cần nhập là số");
            return false;
        }
        if (paramDataOrder.soDienThoai.length < 10 || paramDataOrder.soDienThoai.length > 11) {
            alert("Số điền thoại phải bao gồm 10 hoặc 11 số");
            return false;
        }
        return true;

    }
    // hàm xử lý sự kiện gọi sever tạo đơn hàng
    function callApiPostOrder(paramDataOrder) {
        "use strict";
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramDataOrder),
            success: function (res) {
                alert("Tạo đơn hàng thành công");
                console.log(res);
                resetDataFormCreate(); // gán sự kiện reset thông tin các trường trong form create
                getDataApi();
            },
            error: function () {
                alert("Lỗi");
            }
        })
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm load dữ liệu vào bảng
    function onLoadDataToTable(paramOdersPizza) {
        "use strict";
        var vTable = $("#oder-pizza-table").DataTable();
        vTable.clear();
        vTable.rows.add(paramOdersPizza);
        vTable.draw();
    }
    // hàm lấy danh sách đồ uống
    function getDrinkList() {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            async: false,
            success: function (Response) {
                loadDataDrinkList(Response);
            },
            error: function () {
                alert("Lỗi");
            }
        })
    }
    // hàm load dữ liệu danh sách nước uống vào select
    function loadDataDrinkList(paramResponse) {
        "use strict";
        for (var bI in paramResponse) {
            var vOption = $("<option>", {
                text: paramResponse[bI].tenNuocUong,
                value: paramResponse[bI].maNuocUong
            });
            $("#select-drink").append(vOption)
        }

        for (var bI in paramResponse) {
            var vOption = $("<option>", {
                text: paramResponse[bI].tenNuocUong,
                value: paramResponse[bI].maNuocUong
            });
            $("#select-drink-create").append(vOption)
        }
    }
    // hàm xử lý sự kiện reset thông tin các trường trong form create
    function resetDataFormCreate() {
        "use strict";
        $("#create-modal").modal("hide");
        $("#select-size-create").val("");
        $("#inp-duong-kinh-create").val("");
        $("#inp-suonnuong-create").val("");
        $("#inp-salad-create").val("");
        $("#select-pizza-create").val("");
        $("#inp-voucher-id-create").val("");
        $("#select-drink-create").val("");
        $("#inp-soluongnuocuong-create").val("");
        $("#inp-name-create").val("");
        $("#inp-price-create").val("");
        $("#inp-discount-create").val("");
        $("#inp-phone-create").val("");
        $("#inp-address-create").val("");
        $("#inp-message-create").val("");
        $("#inp-email-create").val("");

    }
    function checkVoucherDiscount(paramVoucher) {
        "use strict";
        if ($("#inp-voucher-id-create").val() == paramVoucher) {
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramVoucher,
                type: "GET",
                async: false,
                success: function (res) {
                    $("#inp-discount-create").val(res.phanTramGiamGia);
                    gDiscount = res.phanTramGiamGia;
                },
                error: function () {
                    $("#inp-discount-create").val("Mã giảm giá không đúng");
                }
            })
        }
    }
})

